# svn-dependencies
> Command-line tool for installing node module dependencies from SVN repositories

# Required steps to use svn-dependencies
* Install svn-dependencies as dev dependency using following command
``` 
npm install -D git+https://dhananjay_nazare@bitbucket.org/dhananjay_nazare/svn-dependencies.git
``` 
* Add .npmrc file at root level with following inputs in your project to avoid creation of  package-lock.json file
```
package-lock=false
```
* Add svnDependencies section in package.json file to register svn dependencies. Also add postinstall script as mentioned in below example,
```
{
  "name": "SampleApp",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "author": "",
  "license": "ISC",
  "scripts": {
    "postinstall": "svn-dependencies install"
  },
  "svnDependencies": {
    "npm-module1": "http://<svn_path>/<zip_of_npm_package>.tgz", //OR
    "npm-module2": "http://<svn_path>/<folder_path_of_npm_package>"
  },
  "devDependencies": {
    "svn-dependencies": "git+https://dhananjay_nazare@bitbucket.org/dhananjay_nazare/svn-dependencies.git",
  },
  "dependencies": {
    "angular": "^1.6.9"
  }
}
```

# TODOs
* Allow user to cache SVN dependencies locally. User should also be able to clean cache. - DONE
* Provide support for npm version syntax. Please refer https://docs.npmjs.com/misc/semver for version syntax to be followed.
* Provide support folder as well as .sgz file path as a SVN path - DONE

##Possible Issues
* Conflict of NPM packages need to be resolved.



