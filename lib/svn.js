const fs = require('fs-extra');
const join = require('path').join;
const spawnSync = require('child_process').spawnSync;
const execSync = require('child_process').execSync;
const sync = require('rimraf').sync;
const logger = require('./logging');
const npm = require('./npm');
const arrayContains = require('./utilities').arrayContains;
const svnPathResolver = require('./svn-path-resolver');
module.exports = { fetch_deps_from_svn_async };

function fetch_deps_from_svn_async(projectRoot) {
    let context = { svnDependencies: [], svnRelations: [] };
    let svnDependencies = npm.get_svn_dependencies(process.cwd());
    return fetch_svn_dependencies(svnDependencies, projectRoot, '', context)
        .then(() => context);
}

function fetch_svn_dependencies(svnDependencies, targetPath, parentModuleName, context) {
    let requests = [];

    for (let moduleName in svnDependencies) {
        let modulePath = join(targetPath, 'node_modules', moduleName);
        let repositoryUrl = svnDependencies[moduleName];
        if (arrayContains(context.svnDependencies, { moduleName })) {
            context.svnRelations.push({ parentModuleName, moduleName })
            logger.info(`${moduleName} is already downloaded!!`);
            continue;
        }
        let request = fetch_svn_module(moduleName, targetPath, repositoryUrl, parentModuleName, context);
        requests.push(request);
    };

    return Promise.all(requests);
}

function fetch_svn_module(moduleName, targetPath, repositoryUrl, parentModuleName, context) {
    let tempPath = join(targetPath, npm.SVN_TEMP_FOLDER);
    let modulePath = join(tempPath, npm.get_module_name(moduleName));
    repositoryUrl = svnPathResolver.resolve(moduleName, repositoryUrl);
    let isZipped = repositoryUrl.endsWith('.tgz');
    let zipOpetaion = isZipped ? decompressModule : compressModule;

    return new Promise((resolve, reject) => {

        zipOpetaion(moduleName, tempPath, repositoryUrl, function (err, zipFileName) {
            if (err) {
                return reject(true);
            }

            context.svnDependencies.push({ moduleName, zipFileName });
            context.svnRelations.push({ parentModuleName, moduleName });

            let packageJsonPath = join(modulePath, 'package');
            if (!fs.existsSync(packageJsonPath)) {
                logger.info(`Package.json file not found for ${moduleName}!! `);
                reject(true);
            }
            let svnDeps = npm.get_svn_dependencies(packageJsonPath);
            fetch_svn_dependencies(svnDeps, targetPath, moduleName, context).then(resolve, reject);
        });
    })
}

function decompressModule(moduleName, tempPath, repositoryUrl, completeCallback) {
    var zipFileName = repositoryUrl.split(/[/]+/).pop();

    logger.info(`Fetching ${moduleName}...`);
    var success = fetch_from_svn(tempPath, zipFileName, repositoryUrl);
    if (success) {
        logger.success(`Fetched ${moduleName} successfully`);
    } else {
        logger.error(`Unable to fetch ${moduleName} from SVN`);
        return completeCallback(true);
    }

    let modulePath = join(tempPath, npm.get_module_name(moduleName));
    npm.decompress({
        src: join(tempPath, zipFileName),
        dest: modulePath
    }, function (error) {
        if (error) {
            logger.error(`Failed to unzip ${zipFileName}`);
        }
        return completeCallback(error, zipFileName);
    });
}

function compressModule(moduleName, tempPath, repositoryUrl, completeCallback) {

    // Ensure the SVN modules folder exists
    if (!fs.existsSync(tempPath))
        fs.mkdirSync(tempPath);

    let modulePath = join(tempPath, npm.get_module_name(moduleName));

    logger.info(`Fetching ${moduleName}...`);
    var success = fetch_from_svn(modulePath, 'package', repositoryUrl);
    if (success) {
        logger.success(`Fetched ${moduleName} successfully`);
    } else {
        logger.error(`Unable to fetch ${moduleName} from SVN`);
        return completeCallback(true);
    }

    npm.compress({
        src: join(modulePath, 'package'),
        dest: tempPath
    }, function (error, zipFileName) {
        if (error) {
            logger.error(`Failed to zip ${moduleName}`);
        }
        return completeCallback(error, zipFileName);
    });
}


function fetch_from_svn(svnModulesPath, moduleName, repositoryUrl) {
    let modulePath = join(svnModulesPath, npm.get_module_name(moduleName))

    if (fs.existsSync(join(modulePath))) {
        return true;
    }

    // Ensure the SVN modules folder exists
    if (!fs.existsSync(svnModulesPath))
        fs.mkdirSync(svnModulesPath)

    try {
        // If the package already exists in the local cache, delete it first
        if (fs.existsSync(modulePath))
            sync(modulePath)
    } catch (err) {
        logger.warn(`Unable to delete ${moduleName}`)
    }

    // Fetch the package from SVN to the local cache
    let svnResult = spawnSync('svn', ['export', '--force', repositoryUrl, moduleName], {
        cwd: svnModulesPath,
        env: process.env,
        stdio: 'pipe',
        encoding: 'utf8'
    })

    // If a SVN error occurred, report the error and skip this package
    if (svnResult.status !== 0) {
        logger.error(`Unable to fetch ${moduleName}`)
        logger.error(svnResult.stderr)
        return false;
    }

    return true;

}
