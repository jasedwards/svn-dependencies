module.exports = { arrayContains, removeItem };

function arrayContains(array, filterObj) {
    if (!array || !filterObj || array.length == 0) {
        return false;
    }

    for (let obj of array) {
        let allKeysMatch = true;
        for (let key in filterObj) {
            if (!obj.hasOwnProperty(key) || obj[key] !== filterObj[key]) {
                allKeysMatch = false;
                break;
            }
        }
        if (allKeysMatch) {
            return true;
        }
    }
    return false;
}

function removeItem(array, item) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] == item) {
            array.splice(i, 1);
            i--; // Prevent skipping an item
        }
    }
}