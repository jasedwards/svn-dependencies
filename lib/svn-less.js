const fs = require('fs-extra');
const rimraf = require('rimraf');
const path = require('path');
const removeItem = require('./utilities').removeItem;
const processPath = process.cwd();

module.exports = { register_svn_less_dependencies };

function register_svn_less_dependencies(svnrelations) {
    let moduleList = [];
    breadth_first_search(build_dependency_tree(svnrelations), moduleList);
    moduleList = moduleList.reverse();
    if (moduleList.length === 0) {
        return;
    }

    let svnLessfilePath = path.join(processPath, 'svnDependencies.less');
    let indexLessfilePath = path.join(processPath, '_index.less');
    rimraf.sync(svnLessfilePath);

    let filedata = '';
    moduleList.forEach(moduleName => filedata += `@import '~${moduleName}/_index.less';\r\n`);
    fs.writeFileSync(svnLessfilePath, filedata);

    if (!fs.existsSync(indexLessfilePath)) {
        fs.createFileSync(indexLessfilePath);
    }

    let rowData = fs.readFileSync(indexLessfilePath)
    if (rowData.indexOf('svnDependencies') <= -1) {
        rowData = `@import './svnDependencies.less';\r\n${rowData}`;
        fs.writeFileSync(indexLessfilePath, rowData);
    }
}

function breadth_first_search(nodes, context) {
    if (!nodes || nodes.length == 0) {
        return;
    }
    nodes.forEach(node => {
        removeItem(context, node.moduleName);
        context.push(node.moduleName);
    });
    nodes.forEach(node => breadth_first_search(node.children, context));
}

function build_dependency_tree(svnrelations) {
    if (!svnrelations || svnrelations.length === 0) {
        return null;
    }

    let rootNodes = svnrelations.filter(m => !m.parentModuleName);
    build_tree(rootNodes, svnrelations);
    return rootNodes;
}

function build_tree(nodes, svnrelations) {
    if (!nodes || nodes.length == 0) {
        return;
    }
    nodes.forEach(node => {
        let childNodes = svnrelations.filter(m => m.parentModuleName == node.moduleName);
        node.children = childNodes;
        build_tree(childNodes, svnrelations);
    });
}
