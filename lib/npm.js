const fs = require('fs-extra');
const path = require('path');
const rimraf = require('rimraf');
const execSync = require('child_process').execSync;
const targz = require('targz');

const logger = require('../lib/logging');
const SVN_TEMP_FOLDER = 'svn_dependencies';

module.exports = { install_dependencies_sync, get_svn_dependencies, compress, decompress, 
    SVN_TEMP_FOLDER, clean_svn_dependencies, npm_install, register_svn_dependencies, 
    get_module_name, remove_scope_folder };

function install_dependencies_sync(moduleInfos, projectRoot, persist_deps_in_package_json) {
    let svnDeps = {};
    moduleInfos.forEach((moduleInfo) => {
        moduleInfo.dependencyUrl = `file:${SVN_TEMP_FOLDER}/${moduleInfo.zipFileName}`;
        svnDeps[moduleInfo.moduleName] = moduleInfo.dependencyUrl;

        install_dependency_sync(moduleInfo, projectRoot);
    });

    if (!persist_deps_in_package_json) {
        remove_svn_dependencies_from_packagejson(projectRoot, svnDeps);
        remove_svn_temp_folder(projectRoot);
    }
}

function install_dependency_sync(moduleInfo, projectRoot) {
    logger.info(`Installing ${moduleInfo.moduleName}...`);
    try {
        execSync(`npm install ${moduleInfo.dependencyUrl}`, {
            cwd: projectRoot,
            env: process.env,
            stdio: 'inherit'
        });
        logger.success(`Installed ${moduleInfo.moduleName} successfully!!`);
    } catch (err) {
        if (err) {
            logger.error(`Unabled to install ${moduleInfo.moduleName}!!`);
            logger.error(err);
        }
    }
}

function get_svn_dependencies(packageFolderPath) {

    //Read package.json file from current directory
    let packageInfo = read_package_info(packageFolderPath);

    // If there are no SVN dependencies, warn and exit
    let svnDependencies = packageInfo.svnDependencies || {}
    if (!Object.keys(svnDependencies).length) {
        logger.warn('Package.json does not contain SVN dependencies');
        return svnDependencies;
    }
    return svnDependencies;
}

function read_package_info(srcPath) {
    // Search for package.json to determine project root
    let packageJsonPath = path.join(srcPath, 'package.json');
    let packageInfo = {};
    let rawData;
    // If no package.json was found, report the error and exit
    if (!packageJsonPath) {
        logger.error('Unable to find package.json');
        return packageInfo;
    }
    try {
        rawData = fs.readFileSync(packageJsonPath, 'utf8')
        packageInfo = JSON.parse(rawData);
    } catch (err) {
        logger.error('LOGGING PACKAGE.JSON DETAILED ERROR');
        logger.error('Source Path: ' + srcPath);
        logger.error('Raw Data: ' + rawData);
        logger.error(err);
        logger.error('Unable to read or parse package.json');
    }
    return packageInfo;
}

function add_svn_dependencies_to_packagejson(srcPath, svnDeps) {
    if (!svnDeps) {
        return;
    }

    var packageInfo = read_package_info(srcPath);
    packageInfo.dependencies = Object.assign((packageInfo.dependencies || {}), svnDeps);

    let packageJsonPath = path.join(srcPath, 'package.json');
    try {
        fs.writeFileSync(packageJsonPath, JSON.stringify(packageInfo, null, '\t'));
    } catch (error) {
        logger.error(error);
        logger.error(`Unable to update package.json with dependency for ${moduleName}`);
    }
}

function add_svn_dependency_to_packagejson(srcPath, moduleName, moduleUrl) {

    var packageInfo = read_package_info(srcPath);
    packageInfo.dependencies = packageInfo.dependencies || {};

    //If dependency is already registered, then return
    if (packageInfo.dependencies[moduleName] == moduleUrl) {
        return;
    }

    let dependency = {};
    dependency[moduleName] = moduleUrl;
    packageInfo.dependencies = Object.assign(packageInfo.dependencies, dependency);

    let packageJsonPath = path.join(srcPath, 'package.json');
    try {
        fs.writeFileSync(packageJsonPath, JSON.stringify(packageInfo, null, '\t'));
    } catch (error) {
        logger.error(error);
        logger.error(`Unable to add depdendencies in package.json!!`);
    }
}

function remove_svn_dependency_from_packagejson(srcPath, moduleName) {

    var packageInfo = read_package_info(srcPath);
    packageInfo.dependencies = packageInfo.dependencies || {};

    //If dependency is already registered, then return
    if (!packageInfo.dependencies[moduleName]) {
        return;
    }

    delete packageInfo.dependencies[moduleName];

    let packageJsonPath = path.join(srcPath, 'package.json');
    try {
        fs.writeFileSync(packageJsonPath, JSON.stringify(packageInfo, null, '\t'));
    } catch (error) {
        logger.error(error);
        logger.error(`Unable to update package.json with dependency for ${moduleName}!!`);
    }
}

function remove_svn_dependencies_from_packagejson(srcPath, svnDeps) {

    if (!svnDeps) {
        return;
    }
    var packageInfo = read_package_info(srcPath);
    packageInfo.dependencies = packageInfo.dependencies || {};
    if (!packageInfo.dependencies || Object.keys(packageInfo.dependencies).length === 0) {
        return;
    }

    Object.keys(svnDeps)
        .filter((name) => packageInfo.dependencies[name] === svnDeps[name])
        .forEach((name) => delete packageInfo.dependencies[name]);

    let packageJsonPath = path.join(srcPath, 'package.json');
    try {
        fs.writeFileSync(packageJsonPath, JSON.stringify(packageInfo, null, '\t'));
    } catch (error) {
        logger.error(error);
        logger.error(`Unable to remove dependencies from package.json!!`);
    }
}

function remove_svn_temp_folder(projectRoot) {
    let tempPath = path.join(projectRoot, SVN_TEMP_FOLDER);
    if (fs.existsSync(tempPath)) {
        rimraf.sync(tempPath);
    }
}

function remove_scope_folder(projectRoot) {
    try {
        let tempPath = path.join(projectRoot, "node_modules", "@clearsight");
        if (fs.existsSync(tempPath)) {
            rimraf.sync(tempPath);
        }
    } catch (err) {
        logger.error(err);
        logger.warn("Failed to remove @clearsight folder!!");
    }
}

function compress(opts, callback) {
    try {
        let result = execSync(`npm pack "${opts.src}"`, {
            cwd: opts.dest,
            env: process.env
        });
        callback(false, result.toString().replace(/(\r\n\t|\n|\r\t)/gm, ''));
    } catch (err) {
        logger.error(err);
        callback(err);
    }
}

function decompress(opts, callback) {
    try {
        return targz.decompress(opts, callback);
        callback();
    } catch (err) {
        logger.error(err);
        callback(err);
    }

}

function clean_svn_dependencies(srcPath) {
    let packageInfo = read_package_info(srcPath);
    if (packageInfo.dependencies) {
        let svnDeps = {};
        Object.keys(packageInfo.dependencies).forEach(function (moduleName) {
            if (packageInfo.dependencies[moduleName].indexOf(`file:${SVN_TEMP_FOLDER}`) > -1) {
                svnDeps[moduleName] = packageInfo.dependencies[moduleName];
            }
        });
        remove_svn_dependencies_from_packagejson(srcPath, svnDeps);
    }
    remove_svn_temp_folder(srcPath);
}

function npm_install(projectRoot) {
    try {
        execSync(`npm install`, {
            cwd: projectRoot,
            env: process.env,
            stdio: 'inherit'
        });
    } catch (err) {
        if (err) {
            logger.error(err);
            return false;
        }
    }
    return true;
}

function register_svn_dependencies(moduleInfos, projectRoot) {

    if (!moduleInfos || moduleInfos.length === 0) {
        return;
    }

    var packageInfo = read_package_info(projectRoot);
    packageInfo.dependencies = packageInfo.dependencies || {};
    moduleInfos.forEach((moduleInfo) => {
        packageInfo.dependencies[moduleInfo.moduleName] = `file:${SVN_TEMP_FOLDER}/${moduleInfo.zipFileName}`;
    });

    let packageJsonPath = path.join(projectRoot, 'package.json');
    try {
        fs.writeFileSync(packageJsonPath, JSON.stringify(packageInfo, null, '\t'));
    } catch (error) {
        logger.error(error);
        logger.error(`Unable to update package.json with SVN dependencies`);
    }

}

function get_module_name(moduleName) {
    if (moduleName.indexOf("/") > 0) {
        return moduleName.split("/")[1];
    }
    return moduleName;
}